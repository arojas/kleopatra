# translation of kwatchgnupg.po to Frysk
# Bram Schoenmakers <bramschoenmakers@kde.nl>, 2004, 2005.
# Rinse de Vries <rinsedevries@kde.nl>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kwatchgnupg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-03 01:38+0000\n"
"PO-Revision-Date: 2007-04-29 14:30+0200\n"
"Last-Translator: Rinse de Vries <rinsedevries@kde.nl>\n"
"Language-Team: Frysk <kde-i18n-fry@kde.org>\n"
"Language: fy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Douwe"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr " "

#: aboutdata.cpp:26
msgid "Steffen Hansen"
msgstr ""

#: aboutdata.cpp:26
msgid "Original Author"
msgstr "Oarspronklike skriuwer"

#: aboutdata.cpp:31
#, kde-format
msgid "KWatchGnuPG"
msgstr "KWatchGnuPG"

#: aboutdata.cpp:33
#, kde-format
msgid "GnuPG log viewer"
msgstr "GnuPG-logwerjefte"

#: aboutdata.cpp:35
#, kde-format
msgid "(c) 2004 Klarälvdalens Datakonsult AB\n"
msgstr ""

#: kwatchgnupgconfig.cpp:57
#, fuzzy, kde-format
#| msgid "Configure KWatchGnuPG"
msgctxt "@title:window"
msgid "Configure KWatchGnuPG"
msgstr "KWatchGnuPG ynstelle"

#: kwatchgnupgconfig.cpp:73
#, kde-format
msgid "WatchGnuPG"
msgstr "WatchGnuPG"

#: kwatchgnupgconfig.cpp:83
#, kde-format
msgid "&Executable:"
msgstr "Ut t&e fieren"

#: kwatchgnupgconfig.cpp:92
#, kde-format
msgid "&Socket:"
msgstr "&Socket:"

#: kwatchgnupgconfig.cpp:101
#, kde-format
msgid "None"
msgstr "Gjin"

#: kwatchgnupgconfig.cpp:102
#, kde-format
msgid "Basic"
msgstr "Basis"

#: kwatchgnupgconfig.cpp:103
#, kde-format
msgid "Advanced"
msgstr "Avansearre"

#: kwatchgnupgconfig.cpp:104
#, kde-format
msgid "Expert"
msgstr "Expert"

#: kwatchgnupgconfig.cpp:105
#, kde-format
msgid "Guru"
msgstr "Guru"

#: kwatchgnupgconfig.cpp:106
#, kde-format
msgid "Default &log level:"
msgstr "Standert &lognivo:"

#: kwatchgnupgconfig.cpp:114
#, kde-format
msgid "Log Window"
msgstr "Logfinster"

#: kwatchgnupgconfig.cpp:126
#, fuzzy, kde-format
#| msgctxt "history size spinbox suffix"
#| msgid " lines"
msgctxt "history size spinbox suffix"
msgid " line"
msgid_plural " lines"
msgstr[0] " regels"
msgstr[1] " regels"

#: kwatchgnupgconfig.cpp:127
#, kde-format
msgid "unlimited"
msgstr "Unbeheind"

#: kwatchgnupgconfig.cpp:128
#, kde-format
msgid "&History size:"
msgstr "&Skiednisgrutte:"

#: kwatchgnupgconfig.cpp:132
#, kde-format
msgid "Set &Unlimited"
msgstr "&Uneinich"

#: kwatchgnupgconfig.cpp:139
#, kde-format
msgid "Enable &word wrapping"
msgstr "&Wurdôfbrekking ynskeakelje"

#: kwatchgnupgmainwin.cpp:79
#, kde-format
msgid "[%1] Log cleared"
msgstr ""

#: kwatchgnupgmainwin.cpp:86
#, kde-format
msgid "C&lear History"
msgstr "&Histoarje wiskje"

#: kwatchgnupgmainwin.cpp:116
#, kde-format
msgid "[%1] Log stopped"
msgstr ""

#: kwatchgnupgmainwin.cpp:133
#, fuzzy, kde-format
#| msgid ""
#| "The watchgnupg logging process could not be started.\n"
#| "Please install watchgnupg somewhere in your $PATH.\n"
#| "This log window is now completely useless."
msgid ""
"The watchgnupg logging process could not be started.\n"
"Please install watchgnupg somewhere in your $PATH.\n"
"This log window is unable to display any useful information."
msgstr ""
"It watchgnupg logproces koe net starten wurde.\n"
"Ynstallearje watchgnupg yn in map dat yn jo paad ($PATH) foarkomt.\n"
"Jo hawwe no neat oan it logfinster."

#: kwatchgnupgmainwin.cpp:136
#, kde-format
msgid "[%1] Log started"
msgstr ""

#: kwatchgnupgmainwin.cpp:171
#, kde-format
msgid "There are no components available that support logging."
msgstr "Der binne gjin ûnderdielen oanwêzich dy't it loggen stypje."

#: kwatchgnupgmainwin.cpp:178
#, kde-format
msgid ""
"The watchgnupg logging process died.\n"
"Do you want to try to restart it?"
msgstr ""
"It watchgnupg logproses is stoppe.\n"
"Wolle jo dizze opnij starte?"

#: kwatchgnupgmainwin.cpp:180
#, kde-format
msgid "Try Restart"
msgstr "Opnij starte"

#: kwatchgnupgmainwin.cpp:181
#, kde-format
msgid "Do Not Try"
msgstr "Net probearje"

#: kwatchgnupgmainwin.cpp:183
#, kde-format
msgid "====== Restarting logging process ====="
msgstr "====== Logproses wurdt opnij opstarten ====="

#: kwatchgnupgmainwin.cpp:187
#, fuzzy, kde-format
#| msgid ""
#| "The watchgnupg logging process is not running.\n"
#| "This log window is now completely useless."
msgid ""
"The watchgnupg logging process is not running.\n"
"This log window is unable to display any useful information."
msgstr ""
"It watchgnupg logproses is net aktyf.\n"
"Jo hawwe no neat oan it logfinster."

#: kwatchgnupgmainwin.cpp:222
#, kde-format
msgid "Save Log to File"
msgstr "Logtriem opslaan nei triem"

#: kwatchgnupgmainwin.cpp:230
#, kde-format
msgid "Could not save file %1: %2"
msgstr ""

#. i18n: ectx: Menu (file)
#: kwatchgnupgui.rc:4
#, kde-format
msgid "&File"
msgstr ""

#. i18n: ectx: ToolBar (mainToolBar)
#: kwatchgnupgui.rc:13
#, kde-format
msgid "Main Toolbar"
msgstr ""

#: tray.cpp:30
#, kde-format
msgid "KWatchGnuPG Log Viewer"
msgstr "KWatchGnuPG Logwerjefte"

#~ msgid ""
#~ "The file named \"%1\" already exists. Are you sure you want to overwrite "
#~ "it?"
#~ msgstr "De triem \"%1\" bestiet al. Wolle jo dizze werklik oerskriuwe?"

#~ msgid "Overwrite File"
#~ msgstr "Triem oerskriuwe"

#~ msgid "Configure KWatchGnuPG..."
#~ msgstr "KWatchGnuPG ynstelle..."

#~ msgid "Overwrite"
#~ msgstr "Oerskriuwe"
